package domains

import (
	"database/sql"
	"time"
)

type Imgobj struct {
	ID         string    `json:"id"`
	CreatedAt  time.Time `json:"-"`
	UpdatedAt  time.Time `json:"-"`
	Name       string    `json:"name"`
	Phone      string    `json:"phone"`
	City       string    `json:"city"`
	Age        int       `json:"age"`
	Sex        string    `json:"sex"`
	Extra      sql.NullString   `json:"extra"`
	Selected   int       `json:"selected"`
	Scope      string    `json:"scope"`
	Lastresult string    `json:"lastresult"`
}

type Imgid struct {
	Imgid string `json:"imgid"`
}