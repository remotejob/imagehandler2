package config

import (
	"database/sql"
	"log"

	"github.com/spf13/viper"
)

type Constants struct {
	PORT   string
	IMGHOSTER string


	Sqlite struct {
		Dblocation string
		Table      string
	}
}

type Config struct {
	Constants
	Database *sql.DB
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	if err != nil {
		return &config, err
	}
	config.Constants = constants
	
	litedb, err := sql.Open("sqlite3", config.Constants.Sqlite.Dblocation)

	if err != nil {
		return &config, err

	}

	config.Database = litedb

	config.Constants = constants
	return &config, err

}

func initViper() (Constants, error) {
	viper.SetConfigName("imagehandler2.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                    // Search the root directory for the configuration file
	err := viper.ReadInConfig()                 // Find and read the config file
	if err != nil {                             // Handle errors reading the config file
		return Constants{}, err
	}
	// viper.WatchConfig() // Watch for changes to the configuration file and recompile
	// viper.OnConfigChange(func(e fsnotify.Event) {
	// 	fmt.Println("Config file changed:", e.Name)
	// })
	viper.SetDefault("PORT", "3002")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
