package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/imagehandler2/internal/config"
	"gitlab.com/remotejob/imagehandler2/internal/domains"
	"gitlab.com/remotejob/imagehandler2/pkg/dbhandler"
	"gitlab.com/remotejob/imagehandler2/pkg/deleteimgfromhoster"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {

	router := chi.NewRouter()

	router.Post("/getimages", config.GetImages)
	router.Post("/delete",config.DeleteImage)

	return router

}

func (conf *Config) GetImages(w http.ResponseWriter, r *http.Request) {

	quant := r.Header.Get("X-QUANT")

	imgs, err := dbhandler.SelectImages(conf.Database, quant)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resbyte, err := json.Marshal(imgs)
	if err != nil {

		log.Fatalln(err)
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(resbyte)

}

func (conf *Config) DeleteImage(w http.ResponseWriter, r *http.Request) {

	var imgid domains.Imgid

	err := json.NewDecoder(r.Body).Decode(&imgid)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	// log.Println("delete id",imgid.Imgid)
	err = dbhandler.DeleteImage(conf.Database,imgid.Imgid)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
	}
	
	err = deleteimgfromhoster.Delete(conf.IMGHOSTER,imgid.Imgid)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write([]byte("{\"res\":\"OK\"}"))

}
