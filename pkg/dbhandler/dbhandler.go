package dbhandler

import (
	"database/sql"
	"log"

	"gitlab.com/remotejob/imagehandler2/internal/domains"
)

func SelectImages(db *sql.DB, quant string) ([]domains.Imgobj, error) {

	var res []domains.Imgobj

	sqlstr := "select ID,Name,Phone,City,Age,Sex,Extra,Scope from imgs order by created_at desc limit " + quant

	rows, err := db.Query(sqlstr)
	if err != nil {
		return nil, err
	}

	for rows.Next() {

		var obj domains.Imgobj

		err = rows.Scan(&obj.ID, &obj.Name, &obj.Phone, &obj.City, &obj.Age, &obj.Sex, &obj.Extra, &obj.Scope)

		if err != nil {
			return nil, err
		}

		res = append(res, obj)

	}

	return res, nil

}

func DeleteImage(db *sql.DB, id string) error {

	log.Println("Delete id",id)

	stmt, err := db.Prepare("delete from imgs where ID=?")
	if err != nil {
		return err
	}
	stmt.Exec(id)

	return nil
}