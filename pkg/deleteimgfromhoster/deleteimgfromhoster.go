package deleteimgfromhoster

import (
	"log"
	"net/http"
)

func Delete(url string, imgid string) error {

	
	dpatch := url+"/"+imgid
	println("urldel", dpatch)


	client := &http.Client{}

	req, err := http.NewRequest("DELETE", dpatch, nil)
	if err != nil {
		// do something
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		// do something
		return err
	}

	log.Println("remote delete", resp.Status)

	return nil

}
